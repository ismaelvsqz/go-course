package main

import (
	"log"
	"math/rand"
	"os"
	"os/signal"
	"time"
)

func appCleanup() {
	log.Println("Cleanup Before Exit.")
}

func main() {
	log.Println("Starting Daemon Test.")

	// create Signal Channel
	signalChannel := make(chan os.Signal, 1)

	// catch all signals since not explicitly listing
	signal.Notify(signalChannel)
	//signal.Notify(signalChannel, syscall.SIGQUIT)

	// method invoked upon seeing signal
	go func() {
		currentSignal := <-signalChannel
		// terminated = stop
		log.Printf("Received Signal: %s", currentSignal)
		appCleanup()
		os.Exit(0)
	}()

	// infinite print loop
	for {
		log.Println("hello")

		// wait random number of milliseconds
		Nsecs := rand.Intn(3000)
		log.Printf("About to sleep %dms before looping again", Nsecs)
		time.Sleep(time.Millisecond * time.Duration(Nsecs))
	}
}
