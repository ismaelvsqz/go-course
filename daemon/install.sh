#!/bin/sh

SERVICE_FILE="daemon.service"

SYSTEMD_DIR="/lib/systemd/system"

cp "$SERVICE_FILE" "$SYSTEMD_DIR"
chmod 644 "$SYSTEMD_DIR"/"$SERVICE_FILE"
chown root.root "$SYSTEMD_DIR"/"$SERVICE_FILE"

systemctl enable "$SERVICE_FILE"
