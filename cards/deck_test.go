package main

import (
	"os"
	"testing"
)

func TestNewDeck(t *testing.T) {
	d := newDeck()

	if len(d) != 16 {
		t.Errorf("Expected deck length of 16, but got %v", len(d))
	}

	if d[0] != "Ace of Spades" {
		t.Errorf("Expected first card of Ace of Spades, but got %v", d[0])
	}

	if d[15] != "Four of Clubs" {
		t.Errorf("Expected last card of Four of Clubs, but got %v", d[15])
	}
}

func TestSaveToFileAndNewDeckFromFile(t *testing.T) {
	os.Remove("_decktesting")

	d1 := newDeck()
	d1.saveToFile("_decktesting")

	d2 := newDeckFromFile("_decktesting")

	if len(d2) != 16 {
		t.Errorf("Expected deck length of 16, but got %v", len(d2))
	}

	os.Remove("_decktesting")
}
