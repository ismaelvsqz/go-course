package main

import (
	"database/sql"
	"time"
)

const (
	selectAll string = "SELECT * FROM person"
	selectOne string = "SELECT * FROM person WHERE id = $1"
	insert    string = "INSERT INTO person (name) VALUES ($1) RETURNING id"
	delete    string = "DELETE FROM person WHERE id = $1"
	update    string = "UPDATE person SET name = $1 WHERE id = $2"
)

// Person storages the user data
type Person struct {
	ID      string
	Name    string
	Age     sql.NullInt64
	Surname sql.NullString
	Created time.Time
}

// FindAll returns all the persons in the DB
func FindAll(db *sql.DB) ([]Person, error) {
	var result []Person

	rows, err := db.Query(selectAll)
	if err != nil {
		return result, err
	}
	defer rows.Close()

	for rows.Next() {
		var p Person
		if err := rows.Scan(&p.ID, &p.Name, &p.Age, &p.Surname, &p.Created); err != nil {
			return result, err
		}
		result = append(result, p)
	}

	return result, nil
}

// FindOne returns only one specific person from the DB
func FindOne(db *sql.DB, id string) (Person, error) {
	var result Person

	rows, err := db.Query(selectOne, id)
	if err != nil {
		return result, err
	}
	defer rows.Close()

	for rows.Next() {
		if err := rows.Scan(&result.ID, &result.Name, &result.Age, &result.Surname); err != nil {
			return result, err
		}
	}

	return result, nil
}

// Create a new record in the DB
func Create(db *sql.DB, p Person) (id string, err error) {
	db.QueryRow(insert, p.Name).Scan(&id)
	return id, nil
}

// Delete a record in the DB
func Delete(db *sql.DB, id string) error {
	_, err := db.Exec(delete)
	return err
}

// Update a record in the DB
func Update(db *sql.DB, p Person) error {
	_, err := db.Exec(update, p.Name, p.ID)
	return err
}
