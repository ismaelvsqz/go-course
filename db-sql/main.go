package main

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

const (
	dataSourceName string = "user=postgres password=123456 dbname=postgres sslmode=disable"
)

func main() {
	db, err := sql.Open("postgres", dataSourceName)
	if err != nil {
		fmt.Println(err)
	}

	persons, err := FindAll(db)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(persons)
}
